import { Component, OnInit } from '@angular/core';
import { LoginService } from "../../login.service";

@Component({
  selector: 'sa-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [LoginService],
})
export class NavbarComponent implements OnInit {
  nombre_empresa = "The Core";
  input;
  constructor(private loginService:LoginService) {

   }

  ngOnInit() {
    this.input = {
      username: "",
      password: "",
    };
  }

  onLogin(){
    this.loginService.loginUser(this.input).subscribe(
      response => {
        console.log(response);
        alert("Usuario, "+this.input.username+" Logged");
      },
      error => console.log("error",error)
    ); 
  } 

}
